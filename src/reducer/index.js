import {combineReducers} from 'redux';
import LoaderReducer from './Loader/LoaderReducer';
import PostListReducer from './Post/PostListReducer';

export default combineReducers({
    LoaderReducer: LoaderReducer,
    PostListReducer: PostListReducer
});
 