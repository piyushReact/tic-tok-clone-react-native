import * as types from '../../action/action.type';
import { PrintLog } from '../../component/PrintLog';

const initialState = {
    data: [],
    dataFetched: false,
    loading: false,
    success : false,
    error: false
  } 

export const LoaderReducer = (state = initialState, action) => {
    switch (action.type) {
       
        case types.LOADING_START:
        PrintLog("start3 reducer")
        return {
            ...state,
           
            loading: true,
            data: action.response
        }
        case types.LOADING_STOP:
        return {
            ...state,
          
            loading: false,
            data: action.response
        }
        default:
            return state;
    }
};
export default LoaderReducer
