import * as types from '../../action/action.type';

const initialState = {
    data: [],
    dataFetched: false,
    loading: false,
    success : false,
    error: false,
    result : [],
    create_event : []
  } 

export const PostListReducer = (state = initialState, action) => {
    console.log("Reducer action payload ==> ",action)
    switch (action.type) {
        case types.POST_LIST_SAGA_SUCCESS:
        return {
            ...state,
                loading: false, 
                data: action.response.data,
            } 
        case types.POST_LIST_SAGA_FAIL:
            return {
                ...state, 
                status: 201, 
                loading: false,  
                data: action.response
            }
      
        default:
            return state;
    }
};
export default PostListReducer;
