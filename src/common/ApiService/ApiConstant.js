// Local server URL

// https://jsonplaceholder.typicode.com/posts?_page=1&_limit=2
// posts?_page=1&_limit=2
export const BASE_URL = "https://jsonplaceholder.typicode.com/";
export const STATUS_SUCCESS = 200
export const API_FETCH_POST = BASE_URL + "posts?_page=1&_limit=20"

export const api_header = {
    headers: {
        'Content-Type': 'application/json',
    }
}