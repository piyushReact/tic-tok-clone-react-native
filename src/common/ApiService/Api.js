import axios from "axios";
const axiosInstance = axios.create();

export const getApiWithoutHeader = (urlStr) => {
  
  return axiosInstance
    .get(urlStr)
    .then(response => {
      return response;
    })
    .catch(error => {
      return error;
    });
}
export const postApiWithoutHeader = (urlStr, params) => {
  
  return axiosInstance
    .post(urlStr, params)
    .then(response => {
      return response;
    })
    .catch(error => {
      return error;
    });
}