import React, { useState } from 'react';
import { Image, Animated, Easing ,View,Text, TouchableOpacity, ActivityIndicator} from 'react-native';
// import IconComment from 'react-native-vector-icons/Octicons';
// import { FontAwesome, AntDesign } from '@expo/vector-icons';
import Video from 'react-native-video';
// import { LinearGradient } from 'expo-linear-gradient';
// import Lottie from 'lottie-react-native';

// import musicFly from '../../../assets/lottie-animations/music-fly.json';
import ModalComment from './CommentModal';
import {
  Container,
  Details,
  Actions,
  User,
  Tags,
  Music,
  MusicBox,
  BoxAction,
  TextAction,
} from './feed.style';

const Feed = ({ play, item }) => {
    const [playTrack, setplayTrack] = useState(false);
    const [modalVisible, setmodalVisible] = useState(false);
    
  const spinValue = new Animated.Value(0); 

  Animated.loop(
    Animated.timing(spinValue, {
      toValue: 1,
      duration: 10000,
      easing: Easing.linear,
      useNativeDriver: true,
    }),
  ).start();

  const rotateProp = spinValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });



  return (
    <>
    
      <View

        style={{
          position: 'absolute',
          left: 0,
          right: 0, 
          top: 0,
          height: '70%',
        }}
      />
      <ModalComment commentPayload={item.commentData} modalVisible={modalVisible} setModalVisible={()=>setmodalVisible(false)}/>
      <Container onPress={()=> setplayTrack(!playTrack)}>
        <Video 
         paused={play || playTrack}
          source={{ uri: item.uri }} 
          rate={1.0}
          volume={1.0}
          isMuted={false}
          resizeMode="cover"
          repeat
          style={{ 
            width: '100%', 
            height: '100%',
          }}
        />
      </Container>
      <Details>
      
        <User>{item.username}</User>
        <Tags>{item.tags}</Tags>
        <MusicBox>
          {/* <FontAwesome name="music" size={15} color="#f5f5f5" /> */}
          <Music>{item.music}</Music>
        </MusicBox>
      </Details>
      <Actions>
        <BoxAction>
          
          <TextAction>{item.likes}</TextAction>
        </BoxAction>
        <TouchableOpacity onPress={()=> setmodalVisible(true)} style={{ backgroundColor: "pink", padding: 5, height: 40, width: 40, borderRadius: 42 }}>
        {/* <Text style={{ fontSize: 18 }}>Comments</Text> */}
        </TouchableOpacity>
        <BoxAction>
          <TextAction>{item.comments}</TextAction>
        </BoxAction>
        <BoxAction>
         
          <TextAction>Share</TextAction>
        </BoxAction>
        <BoxAction>
          <Animated.View
            style={{
              borderRadius: 50,
              borderWidth: 12,
              borderColor: '#292929',
              transform: [
                {
                  rotate: !play ? rotateProp : 0,
                },
              ],
            }}
          >
            <Image
              style={{
                width: 35,
                height: 35,
                borderRadius: 25,
              }}
              source={{
                uri: 'https://avatars3.githubusercontent.com/u/45601574',
              }}
            />
          </Animated.View>
             
        </BoxAction>
      </Actions>
      <View
        style={{
          position: 'absolute',
          left: 0,
          right: 0,
          bottom: 0,
          height: '50%',
        }}
      />
    </>
  );
};

export default Feed;