import React, {Component} from 'react';
import {Modal,FlatList, Text, TouchableHighlight,TouchableOpacity, View, Alert} from 'react-native';

export default class ModalComment extends Component {
  state = {
    modalVisible: this.props.modalVisible,
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

    
    renderItem = ({ item }) => (
        <TouchableOpacity style={{ padding: 10 ,  flexDirection: "row"}}>
         <View style={{ height: 40, width: 40, backgroundColor: "red" , borderRadius: 42}}>
            </View>
            <View style={{ justifyContent: "flex-start",paddingHorizontal: 10, width: "72%"}}>
        <Text >{item.name}</Text>
        <Text style={{ fontWeight: "600" }} numberOfLines={2}>{item.message}</Text>
        {
            item.nestedComment.length > 0 && (
                <View style={{ paddingVertical: 2 }}>
            
        <View style={{ paddingVertical: 10, flexDirection: "row"}}>
         <View style={{ height: 30, width: 30,  backgroundColor: "red" , borderRadius: 30}}>
            </View>
            <View style={{ justifyContent: "flex-start",paddingHorizontal: 10, width: "72%"}}>
        <Text >{item.nestedComment[item.nestedComment.length-1].name}</Text>
        <Text style={{ fontWeight: "600" }} numberOfLines={2}>{item.nestedComment[item.nestedComment.length-1].message}</Text>
     
        </View>
        
        {
            <View style={{ width: "36.4%"}}>
            <View style={{ position: "absolute",right: 0, height: 40, width: 40,  backgroundColor: "red" , borderRadius: 42}}>
            </View>
            </View>
        }
        </View>   


                    <Text>{"View replies ("+item.nestedComment.length+")"}</Text>
                    </View>
            )
        }
        </View>
        <View style={{  height: 40, width: 40, backgroundColor: "red" , borderRadius: 42}}>
            </View>
        </TouchableOpacity>
        
    );

  render() {
      console.log("comment payload ==> ",this.props.commentPayload)
      const payloadComment = this.props.commentPayload;
      const totalComment = this.props.commentPayload.length;

    return (
    //   <View style={{marginTop: 22}}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.props.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <TouchableHighlight style={{ flex: 1 }} onPress={() => {
                  this.props.setModalVisible(!this.props.modalVisible);
                }}> 
            <TouchableOpacity style={{ borderTopLeftRadius: 10,borderTopRightRadius: 10,
                backgroundColor: "#fff",width: "100%",height: "60%", position: "absolute", bottom: 0}}>
              <Text onPress={()=> this.props.setModalVisible(!this.props.modalVisible)} style={{ position: "absolute", right: 10,top : 10 }}>close</Text> 
              <Text style={{fontWeight: "600", position: "absolute",top : 10, alignSelf: "center" }}>{totalComment+ " comments"}</Text>
              <FlatList 
                data={payloadComment}
                style={{ marginTop: 40, flex: 1 }}
                renderItem={this.renderItem}
                keyExtractor={item => item.id}
            />
            </TouchableOpacity>
          </TouchableHighlight>
        </Modal>

       
    );
  }
}