import styled from 'styled-components/native';


export const Container = styled.View`
  flex: 1;
  background: #000;
`;

export const Separator = styled.Text`
  color: #fff;
  font-size: 15px;a
  opacity: 0.2;
`;

export const Header = styled.View`
  height: 10%;
  flex-direction: row;
  position: absolute;
  align-self: center;
  z-index: 10;
  align-items: center;
  margin-top: 5%;
`;
export const Text = styled.Text`
  color: #fff;
  font-size: ${(props) => (props.active ? '20px' : '18px')};
  padding: 5px;
  font-weight: bold;
  opacity: ${(props) => (props.active ? '1' : '0.5')};
`;

export const Tab = styled.TouchableOpacity.attrs({
  activeOpacity: 1,
})``;

export const Feed = styled.View`
  flex: 1;
  z-index: -1;
  position: absolute;
`;







// import { StyleSheet } from 'react-native';
// import { colorPrimary } from '../../../../common/Colors'

// export default StyleSheet.create({
//   MainView: { 
//     flex: 1,
//     backgroundColor: "red"
//   },
//   item : {
//       padding: 10,
//   },
//   title: {
//       fontSize: 18
//   }
// });