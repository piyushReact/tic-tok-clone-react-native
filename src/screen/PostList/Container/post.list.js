
import React, { useState } from 'react';
import { View } from 'react-native';

import ViewPager from '@react-native-community/viewpager';

import server from '../../../../Feed';
import Feed from '../Container/Feed';

import { Container, Header, Text, Tab, Separator } from './native.style';

const Home = () => {
  const [tab, setTab] = useState(1);
  const [active, setActive] = useState(0);
  return (
    <Container> 
      <Header>
        <Tab onPress={() => setTab(1)}>
          <Text active={tab === 1}>Following</Text>
        </Tab>
        {/* <Separator >|<> */}
        <Tab onPress={() => setTab(2)}>
          <Text active={tab === 2}>For You</Text>
        </Tab>
      </Header>
      <ViewPager
        onPageSelected={e => {
          setActive(e.nativeEvent.position);
        }}
        orientation="vertical" 
        style={{ flex: 1 }}
        initialPage={0}
      >
        {server.map(item => ( 

          <View key={item.id}>
            <Feed item={item} play={Number(item.id) !== active} />
           </View>
        ))}
      </ViewPager>
    </Container>
  );
};

export default Home;






















// import React, { Component } from 'react'
// import { Text, View , FlatList, StyleSheet,SafeAreaView, TouchableOpacity} from 'react-native'
// // import Styles from './native.style';
// import * as Navigation from '../../../navigation/NavigationService';

// export default class PostList extends Component {
//     constructor(props){
//         super(props)
//         this.state={
          
//         }
//     }
//     componentDidMount(){
//       this.props.FETCH_POST() //fetch post
//     }

//      renderItem = ({ item }) => (
//       <TouchableOpacity 
//       onPress={()=> {
//         this.props.navigation.navigate("PostComment")}}
//       style={{ padding: 20, borderRadius: 10, borderWidth: 1, margin: 10 }}>
//         <Text>{item.title}</Text>
//         </TouchableOpacity>
//       );

//     render() {
//         return (
//             <View style={{ flex: 1 }}>
//               <SafeAreaView>
//                 {
//                   <FlatList
//                   data={this.props.PostListReducer.PostListReducer.data && this.props.PostListReducer.PostListReducer.data}
//                   renderItem={this.renderItem}
//                   keyExtractor={item => item.id}
//                 />
//                 }
//                 </SafeAreaView>
//             </View>
//         )
//     }
// }



