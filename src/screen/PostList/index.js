import { connect } from "react-redux";
import Component from './Container/post.list'
import { FETCH_POST ,LOADING_START, LOADING_STOP } from '../../action'

const mapStateToProps = state => {
  return {
    PostListReducer : {
        PostListReducer : state.PostListReducer
    }
  };
}
const mapDispatchToProps = dispatch => ({
    FETCH_POST: payload => {
      dispatch(FETCH_POST(payload))
  },
  LOADING_START: payload => {
      dispatch(LOADING_START())
  },
  LOADING_STOP: () => {
      dispatch(LOADING_STOP())
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(Component);







