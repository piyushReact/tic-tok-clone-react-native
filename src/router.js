import React, { Component } from "react";
import { View } from "react-native";
import { createAppContainer } from "react-navigation";
import { createRootNavigator } from "./navigation";
import * as NavigationService from './navigation/NavigationService'
// import Loader from "./components/Loader";

class IndexRouter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false,
      isReady: false,
      isStayHere: false,
    };
  }

  componentDidMount() {
    this.isMounted = true;
    NavigationService.setNavigator(this.navigator);
    // this.checkLoggedin();
  }
  componentDidUpdate() {
    NavigationService.setNavigator(this.navigator);
  }
  componentWillUnmount() {
    NavigationService.setNavigator(this.navigator);
  }

  render() {
    const { isLogin, isReady, isStayHere } = this.state;
    // if (!isStayHere) {
    //   return null
    // }
    const RootContainer = createAppContainer(createRootNavigator(true));
    return (
      <View style={{ flex: 1 }}>
        <RootContainer ref={nav => {
          this.navigator = nav;
        }} />
        {/* <Loader /> */}
      </View>
    );
  }
}


export default IndexRouter