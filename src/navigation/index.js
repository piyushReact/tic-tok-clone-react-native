import React, { Component } from "react";
import { createSwitchNavigator } from "react-navigation";
import HomeRoot from '../navigation/HomeRoot';

export const createRootNavigator = (isLogin) => {
  return createSwitchNavigator(
    {
      HomeRoot: { screen: HomeRoot() } 
    },
    {
      initialRouteName: (!isLogin) ? "HomeRoot" : "HomeRoot"
    }
  );
};
 