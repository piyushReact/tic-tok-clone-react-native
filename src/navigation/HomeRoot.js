import React from 'react';
import { Dimensions } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { navigationConstants } from './NavigationConstant'

import PostList from '../screen/PostList';
import PostComment from '../screen/PostList/Container/postComment/postComment';
 
const PostStackHomePage = createStackNavigator(
    {
      [navigationConstants.PostList]: { screen: PostList }, 
      [navigationConstants.PostComment]: { screen: PostComment }, 
    },
    {
      initialRouteName: navigationConstants.PostList,
      headerMode: 'none',
      // mode: 'modal',
      // defaultNavigationOptions: { header: null, gestures: {}, gesturesEnabled: false, swipeEnabled: false, }
    }
  );

export default HomeRoot = () => {
  return createStackNavigator(
    {
        PostStackHomePage 
    },
    {
      initialRouteName: "PostStackHomePage",
      headerMode: 'none',
      mode: 'card',
      defaultNavigationOptions: { header: null, gestures: {}, gesturesEnabled: false, swipeEnabled: false, }
    }
  )
}