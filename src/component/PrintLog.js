/**
 * The method will use to console log
 * @param {*} name = Hint or message to recognize log
 * @param {*} value = Value we want to console
 */ 
export function PrintLog(name, value) {
    let isDebugBuild = false;
    if (__DEV__) {
        return console.log(name, value);
    }
};