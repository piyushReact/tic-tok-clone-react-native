import * as type from './action.type'
// import { PrintLog } from '../components/PrintLog';

 // FETCH_POST
 export const FETCH_POST = (payload) => {
  return {
    type: type.POST_LIST,
    payload : payload 
  }
};

export const LOADING_START = (payload) => {
    return {
      type: type.LOADING_START,
      payload : payload 
    }
  };
 
  export const LOADING_STOP = (payload) => {
    return {
      type: type.LOADING_STOP,
      payload : payload 
    }
  };
