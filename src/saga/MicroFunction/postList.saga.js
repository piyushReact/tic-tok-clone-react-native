import { put, call } from 'redux-saga/effects';
import { getApiWithoutHeader } from '../../common/ApiService/Api'
import { API_FETCH_POST } from '../../common/ApiService/ApiConstant'
import * as types from '../../action/action.type'



export function* POST_LIST_SAGA(payload) {
  const response = yield call(getApiWithoutHeader, API_FETCH_POST)
  console.log("response check saga ==> ",response)
  try {
    if (response.status == "200") {
      yield put({ type: types.POST_LIST_SAGA_SUCCESS, response });
    
    } else {
      yield put({ type: types.TERM_CONDI_SAGA_FAIL, response });
    }
  } catch (e) {
    console.log("from catch block :==> ", e)
  }
}
