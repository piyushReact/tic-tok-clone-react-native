import { takeLatest } from 'redux-saga/effects';
import { POST_LIST_SAGA } from './MicroFunction/postList.saga';

import * as types from '../action/action.type';

export default function* watchUserAuthentication() {

  yield takeLatest(types.POST_LIST, POST_LIST_SAGA);

}