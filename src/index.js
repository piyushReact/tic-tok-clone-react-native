import React, { Component } from 'react'
import { Provider } from 'react-redux'
import RootContainer from './router'
import configureStore from './store'

const store = configureStore()

export default class Index extends Component{
  render() {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    )
  }
} 


// create list of post

